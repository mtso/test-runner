var testPath = './test/http';

(function() {

  const createHttpTest = require(testPath);

  const test = createHttpTest({
    baseUrl: 'https://google.com',
  });
  
  test.run()
    .then((results) => {
      console.log(JSON.stringify(results, null, 2));
    })
    .catch(console.error);
  
})();
