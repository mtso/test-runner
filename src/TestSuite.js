function TestSuite(tests, params) {
  this._params = params || {};
  this._tests = tests || [];
  this._start = null;
  this._end = null;
}

TestSuite.prototype.run = function run() {
  this._start = Date.now();

  try {
    return Promise.all(this._runTests())
      .then(this._renderResults.bind(this));
  } catch(err) {
    return new Promise((resolve, reject) => {
      reject(err);
    });
  }
};

TestSuite.prototype._runTests = function _startTests() {
  return this._tests.map(promisifyTest);
}

function promisifyTest(test) {
  return new Promise((resolve, reject) => {
    const done = (err) => resolve({
      error: err || null,
      message: err && err.message || null,
      description: test.description,
      isSuccess: !err,
    });

    try {
      test.test(done);
    } catch(err) {
      done(err);
    }
  });
}

TestSuite.prototype._renderResults = function _renderResults(results) {
  this._end = Date.now();
  const elapsedTime = this._start && (this._end - this._start) || 'error';

  return {
    name: this._params && this._params.name,
    baseUrl: this._params && this._params.baseUrl,
    failCount: results.filter((t) => !t.isSuccess).length,
    elapsedTime,
    results,
  };
};

module.exports = TestSuite;
