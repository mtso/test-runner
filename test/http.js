const request = require('superagent');
const expect = require('chai').expect;
const TestSuite = require('../src/TestSuite');

module.exports = function createHttpTest(params) {
  params = params || {};
  const baseUrl = params.baseUrl;
  const name = params.name = 'HTTP test';

  const tests = [
    {
      description: 'root is found',
      test: (done) => {
        request.get(baseUrl)
          .then((resp) => {
            expect(resp.status).to.eq(200);
            expect(resp.text).to.exist;
          })
          .then(done)
          .catch(done);
      },
    },
    {
      description: 'not found route',
      test: (done) => {
        request.get(baseUrl + '/notfound')
          .then(done)
          .catch((err) => {
            expect(err).to.exist;
            expect(err.message).to.eq('Not Found');
            done();
          })
          // THIS IS IMPORTANT
          // Always follow up any blocks with `expect` in it
          // with another catch(done) to catch  potential exceptions!
          .catch(done);
      },
    },
  ];
  
  return new TestSuite(tests, params);
};
