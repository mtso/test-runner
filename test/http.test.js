import request from 'superagent';

describe('test', () => {
  test('can make request', (done) => {
    request.get('https://google.com')
      .then((resp) => {
        expect(resp.status).toBe(200);
        expect(resp.text).toBeDefined();
      })
      .then(done)
      .catch(done);
  });

  test('is not found', (done) => {
    request.get('https://google.com/notfound')
      .then(done)
      .catch((err) => {
        expect(err).toBeDefined();
        expect(err.message).toBe('Not Found');
        done();
      });
  });
});
